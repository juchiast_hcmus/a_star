# Please use python 3

import sys
import math
import pygame
import visualize as viz
import a_star
from a_star import Graph, DefaultHeuristic

class Drawer:
    def __init__(self, size, path):
        try:
            self.graph = Graph.read_file(path)
        except Exception:
            size = int(size)
            if size <= 0:
                raise "size must > 0"
            self.graph = Graph(size, (0, 0), (size - 1, size - 1), [])

        self.viz = viz.Visualizer(self.graph, DefaultHeuristic(self.graph))
        self.path = path

    def to_pos(self, screen_pos):
        y, x = screen_pos
        i = math.floor(x / self.viz.grid_size)
        j = math.floor(y / self.viz.grid_size)
        return (i, j)

    def save(self):
        with open(self.path, "w") as f:
            s = []
            s.append(str(self.graph.n))
            x, y = self.graph.start
            s.append("%d %d" % (x, y))
            x, y = self.graph.goal
            s.append("%d %d" % (x, y))
            s += [" ".join(str(int((i, j) in self.graph.blocks)) for j in range(self.graph.n)) for i in range(self.graph.n)]
            f.write("\n".join(s) + "\n")


    def run(self):
        self.viz.init_ui()
        state = "none"
        while True:
            for ev in pygame.event.get():
                if ev.type == pygame.QUIT:
                    pygame.display.quit()
                    pygame.quit()
                    sys.exit()
                elif ev.type == pygame.KEYDOWN:
                    if ev.key in [pygame.K_LCTRL, pygame.K_RCTRL]:
                        state = "ctrl"
                    elif ev.key in [pygame.K_RSHIFT, pygame.K_LSHIFT]:
                        state = "shift"
                    elif ev.key in [pygame.K_LALT, pygame.K_RALT]:
                        state = "alt"
                elif ev.type == pygame.KEYUP:
                    if ev.key in [pygame.K_LCTRL, pygame.K_RCTRL] and state == "ctrl":
                        state = "none"
                    elif ev.key in [pygame.K_RSHIFT, pygame.K_LSHIFT] and state == "shift":
                        state = "none"
                    elif ev.key in [pygame.K_LALT, pygame.K_RALT] and state == "alt":
                        state = "none"
                    elif ev.key == pygame.K_SPACE:
                        self.save()
                elif ev.type == pygame.MOUSEMOTION:
                    if ev.buttons[0] == 1:
                        pos = self.to_pos(ev.pos)
                        if state == "none":
                            self.graph.blocks.add(pos)
                        elif state == "ctrl":
                            if pos in self.graph.blocks:
                                self.graph.blocks.remove(pos)
                        elif state == "shift":
                            self.graph.start = pos
                        elif state == "alt":
                            self.graph.goal = pos

            self.viz.paint()
            self.viz.clock.tick(30)



if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Wrong command line argument")
        sys.exit(1)

    draw = Drawer(sys.argv[2], sys.argv[1])
    draw.run()
