# Please use python 3

import ara
import math
import sys
import pygame
import itertools
import pygame.surfarray as surf
from pygame.locals import *
from pygame.draw import *
import a_star
from a_star import Graph, DefaultHeuristic

c_normal = (255, 255, 255)
c_start = (0, 0, 255)
c_goal = (255, 0, 0)
c_block = (0, 0, 0)
c_border = (0, 0, 0)
c_closed = (200, 200, 200)
c_open = (0, 200, 0)
c_incons = (128, 0, 128)
c_not_found = (255, 0, 0)
c_path_0 = (255, 0, 0)
c_path_1 = (0, 0, 255)
c_just_changed = (255, 200, 0)

size = 600

fps = 60

class Visualizer:
    def __init__(self, graph, heuristic, ep=None):
        self.counter = 0

        self.graph = graph
        self.heuristic = heuristic
        self.ep = ep

        self.OPEN = set()
        self.CLOSED= set()
        self.INCONS = set()
        self.path = set()
        self.just_changed = None

        self.grid_size = math.floor(size / graph.n)
        self.border_size = max(1, math.floor(self.grid_size / 15))

        self.big_font = None
        self.fc_not_found = None
        self.screen = None
        self.clock = None

    def init_ui(self):
        if self.screen is None:
            pygame.init()
            self.screen = pygame.display.set_mode((size, size))
            pygame.display.set_caption('A* Algorithm visualizer')
            self.clock = pygame.time.Clock()
            self.big_font = pygame.font.Font('./Ubuntu-R.ttf', math.floor(size / 9))
            self.fc_not_found= self.big_font.render("NOT FOUND", 2, c_not_found)

    def get_runner(self):
        if self.ep is None:
            return a_star.a_star(self.graph, self.heuristic)
        else:
            return ara.ara(self.graph, self.heuristic, self.ep)

    def run(self):
        self.init_ui()
        
        self.OPEN = set()
        self.CLOSED= set()
        self.INCONS = set()
        self.path = set()
        self.just_changed = None

        self.paint()

        for op, res in self.get_runner():
            for ev in pygame.event.get():
                if ev.type == pygame.QUIT:
                    pygame.display.quit()
                    pygame.quit()
                    sys.exit()

            self.just_changed = None
            if op == a_star.STEP:
                op, u = res
                if op == a_star.ADD_OPEN:
                    self.OPEN.add(u)
                    self.just_changed = u
                elif op == a_star.ADD_CLOSED:
                    self.CLOSED.add(u)
                elif op == a_star.ADD_INCONS:
                    self.INCONS.add(u)
                    self.just_changed = u
                elif op == a_star.UPDATE:
                    self.OPEN = set(u)
                    self.CLOSED = set()
                    self.INCONS = set()
            elif op == a_star.FOUND:
                self.path = set(res.path)
            elif op == a_star.NOT_FOUND:
                self.path = None

            self.paint()
            self.clock.tick(fps)

        while True:
            for ev in pygame.event.get():
                if ev.type == pygame.QUIT:
                    pygame.display.quit()
                    pygame.quit()
                    sys.exit()

            self.paint()

            self.clock.tick(fps)


    def paint(self):
        self.counter += 1
        rect(self.screen, c_normal, (0, 0, size, size))

        color = {pos: c_normal for pos in itertools.product(range(self.graph.n), repeat=2)}
        for pos in self.graph.blocks:
            color[pos] = c_block
        for pos in self.OPEN:
            color[pos] = c_open
        for pos in self.CLOSED:
            color[pos] = c_closed
        for pos in self.INCONS:
            color[pos] = c_incons
        if self.just_changed is not None:
            color[self.just_changed] = c_just_changed
        if self.path is not None:
            for pos in self.path:
                if math.floor(self.counter / 20) % 2 == 0:
                    color[pos] = c_path_0
                else:
                    color[pos] = c_path_1
        color[self.graph.start] = c_start
        color[self.graph.goal] = c_goal

        for (i, j), color in color.items():
            x = i * self.grid_size
            y = j * self.grid_size
            rect(self.screen, c_border, (y, x, self.grid_size, self.grid_size), self.border_size)
            rect(self.screen, color, (y + self.border_size, x + self.border_size, self.grid_size - self.border_size, self.grid_size - self.border_size))

        if self.path is None:
            self.screen.blit(self.fc_not_found, (0, 0))

        pygame.display.update()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Wrong command line argument")
        sys.exit(1)
    graph = Graph.read_file(sys.argv[1])
    h = DefaultHeuristic(graph)
    ep = float(sys.argv[2]) if len(sys.argv) >= 3 else None
    viz = Visualizer(graph, h, ep)
    viz.run()
