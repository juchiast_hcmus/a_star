# Use python 3

import sys
import math
import heapq
import itertools


STEP = "STEP"
NOT_FOUND = "NOT_FOUND"
FOUND = "FOUND"
ADD_CLOSED = "ADD_CLOSED"
ADD_INCONS = "ADD_INCONS"
ADD_OPEN = "ADD_OPEN"
UPDATE = "UPDATE"

def euclide_distance(a, b):
    x, y = a
    x1, y1 = b
    return math.sqrt((x - x1)**2 + (y - y1)**2)

class Graph:
    def read_file(input_path):
        with open(input_path, "r") as f:
            n, = [int(x) for x in f.readline().split()]
            sx, sy, = [int(x) for x in f.readline().split()]
            gx, gy, = [int(x) for x in f.readline().split()]
            blocks = []
            for row in range(n):
                blocks += [(row, col) for (col, x) in enumerate(f.readline().split()) if int(x) == 1]

            return Graph(n, (sx, sy), (gx, gy), blocks)

    def __init__(self, n, start, goal, blocks):
            self.n = n;
            self.start = start
            self.goal = goal
            self.blocks = set((x, y) for x, y in blocks if 0 <= x < n and 0 <= y < n)
            self.direction = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]

    def cost(self, a, b):
        return euclide_distance(a, b)

    def next(self, node):
        if node in self.blocks:
            return []
        x, y = node
        l = [(x + i, y + j) for i, j in self.direction]
        l = [(x, y) for x, y in l if 0 <= x < self.n and 0 <= y < self.n]
        return [(n, self.cost(node, n)) for n in l if n not in self.blocks]

class Path:
    def __init__(self, path, cost, ep, ep1):
        self.cost = cost
        self.length = len(path)
        self.path = path
        self.ep = ep
        self.ep1 = ep1
    def __str__(self):
        head = " ".join(["cost: " + str(self.cost), "ep: " + str(self.ep), "ep': " + str(self.ep1)])
        path = " ".join("(%d, %d)" % (x, y) for x, y in self.path)
        return head + "\n" + path

def do_backtrack(graph, backtrack, goal, ep, ep1):
    path = []
    while goal is not None:
        path.append(goal)
        goal = backtrack[goal]
    path.reverse()
    cost = 0
    for i in range(len(path) - 1):
        cost += graph.cost(path[i], path[i+1])
    return Path(path, cost, ep, ep1)

def a_star(graph, heuristic):
    trace = dict()
    trace[graph.start] = None
    g = dict()
    g[graph.start] = 0
    g[graph.goal] = math.inf
    OPEN = []
    CLOSED = set()
    counter = itertools.count()

    def insert_open(u):
        heapq.heappush(OPEN, (g[u] + heuristic(u), next(counter), u))

    insert_open(graph.start)

    while len(OPEN) > 0:
        _, _, s = heapq.heappop(OPEN)
        if s in CLOSED:
            continue
        CLOSED.add(s)
        yield (STEP, (ADD_CLOSED, s))
        if s == graph.goal:
            yield (FOUND, do_backtrack(graph, trace, s, 1, 1))
            return
        for u, cost in graph.next(s):
            if u not in g:
                g[u] = math.inf
            if g[u] > g[s] + cost:
                trace[u] = s
                g[u] = g[s] + cost
                insert_open(u)
                yield (STEP, (ADD_OPEN, u))

    yield (NOT_FOUND, "")


def format_result(graph, result):
    if result is None:
        return "-1"
    s = []
    s.append(str(result.length))
    s.append(" ".join("(%d, %d)" % (x, y) for x, y in result.path))
    path = set(result.path)

    def to_str(pos):
        if pos == graph.start:
            return "S"
        elif pos == graph.goal:
            return "G"
        elif pos in path:
            return "x"
        elif pos in graph.blocks:
            return "o"
        else:
            return "-"

    for x in range(graph.n):
        s.append(" ".join(to_str((x, y)) for y in range(graph.n)))
    return "\n".join(s)


def run_a_star(graph, heuristic):
    for op, res in a_star(graph, heuristic):
        if op == STEP:
            continue
        elif op == NOT_FOUND:
            return None
        elif op == FOUND:
            return res
        else:
            raise "Error"
    raise "Error"


class DefaultHeuristic:
    def __init__(self, graph):
        self.goal = graph.goal

    def __call__(self, node):
        return euclide_distance(node,  self.goal)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Wrong command line arguments")
        sys.exit(1)
    graph = Graph.read_file(sys.argv[1])
    path = run_a_star(graph, DefaultHeuristic(graph))
    if len(sys.argv) >= 3:
        with open(sys.argv[2], "w") as f:
            f.write(format_result(graph, path) + "\n")
    else:
        print(format_result(graph, path))
