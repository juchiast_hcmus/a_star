import sys
import a_star
import math
import itertools
import heapq
import threading
import queue

def ara(graph, heuristic, ep):
    trace = dict()
    trace[graph.start] = None
    g = dict()
    g[graph.start] = 0
    g[graph.goal] = math.inf
    OPEN = []
    CLOSED = set()
    INCONS = []
    counter = itertools.count()

    def fvalue(node):
        return g[node] + ep * heuristic(node)
    def to_tuple(node):
        return ((fvalue(node), next(counter), node))
    def min_fvalue():
        f, _, _ = OPEN[0]
        return f
    def ImprovePath():
        while len(OPEN) > 0 and fvalue(graph.goal) > min_fvalue():
            _, _, s = heapq.heappop(OPEN)
            if s in CLOSED:
                continue
            CLOSED.add(s)
            yield (a_star.STEP, (a_star.ADD_CLOSED, s))
            for u, cost in graph.next(s):
                if u not in g:
                    g[u] = math.inf
                if g[u] > g[s] + cost:
                    trace[u] = s
                    g[u] = g[s] + cost
                    if u not in CLOSED:
                        heapq.heappush(OPEN, to_tuple(u))
                        yield (a_star.STEP, (a_star.ADD_OPEN, u))
                    else:
                        INCONS.append(u)
                        yield (a_star.STEP, (a_star.ADD_INCONS, u))

    heapq.heappush(OPEN, to_tuple(graph.start))

    for x in ImprovePath():
        yield x
    if g[graph.goal] == math.inf:
        yield (a_star.NOT_FOUND, None)
        return
    union = INCONS + [s for _, _, s in OPEN]
    ep1 = min(ep, g[graph.goal] / min(g[s] + heuristic(s) for s in union))
    yield (a_star.FOUND, a_star.do_backtrack(graph, trace, graph.goal, ep, ep1))

    while ep1 > 1:
        ep = max(ep - 0.5, 1)
        OPEN = [to_tuple(u) for _, _, u in OPEN] + [to_tuple(u) for u in INCONS]
        heapq.heapify(OPEN)
        INCONS = []
        CLOSED = set()
        yield (a_star.STEP, (a_star.UPDATE, [u for _, _, u in OPEN]))
        for x in ImprovePath():
            yield x
        union = INCONS + [s for _, _, s in OPEN]
        ep1 = min(ep, g[graph.goal] / min(g[s] + heuristic(s) for s in union))
        yield (a_star.FOUND, a_star.do_backtrack(graph, trace, graph.goal, ep, ep1))

class Worker(threading.Thread):
    def __init__(self, graph, heuristic, ep, queue, stop):
        threading.Thread.__init__(self)
        self.ara = ara(graph, heuristic, ep)
        self.queue = queue
        self.stop = stop
    def run(self):
        for tag, x in self.ara:
            if not self.stop.empty():
                break
            if tag == a_star.FOUND or tag == a_star.NOT_FOUND:
                self.queue.put((tag, x))

def run_ara(graph, heuristic, ep, time):
    q = queue.Queue()
    stop = queue.Queue()
    w = Worker(graph, heuristic, ep, q, stop)
    w.start()
    w.join(time / 1000)
    stop.put(1)
    if q.empty():
        print("Haven't found anything yet")
    while not q.empty():
        tag, x = q.get()
        print(tag, x)
    sys.exit(0)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Wrong command line arguments")
        sys.exit(1)
    graph = a_star.Graph.read_file(sys.argv[1])
    ep = float(sys.argv[2])
    time = float(sys.argv[3])
    run_ara(graph, a_star.DefaultHeuristic(graph), ep, time)
